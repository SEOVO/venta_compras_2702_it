# -*- encoding: utf-8 -*-
{
    'name': 'Costos en Ventas',
    'category': 'sale',
    'author': 'ITGRUPO',
    'depends': ['purchase','sale_management'],
    'version': '1.0',
    'description':"""
    Modulo que agrega un reporte en compras
    """,
    'auto_install': False,
    'demo': [],
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml'
        ],
    'installable': True
}

