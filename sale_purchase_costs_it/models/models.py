# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.addons.payment.models.payment_acquirer import ValidationError
class ventas_costos(models.Model):
    _name = 'ventas.costos'
    _description = 'modelos costos'
    proveedor    = fields.Many2one('res.partner',required=True)
    producto     = fields.Many2one('product.product',required=True)
    cantidad     = fields.Float(required=True)
    moneda       = fields.Many2one('res.currency',required=True)
    costo_unitario = fields.Float(required=True)
    costo_total    = fields.Float(compute="get_total")
    descuento      = fields.Float('Desc. %')
    fact_import    = fields.Float('Factor de Importacion')
    utilidad       = fields.Float()
    observaciones  = fields.Char()
    sale_id = fields.Many2one('sale.order')
    purchase_id = fields.Many2one('purchase.order',string="OC")

    _sql_constraints = [
         ('unique_id_prod_purchase', 'unique(producto, proveedor,sale_id)', 'There can be no duplication ')
	]
    def get_total(self):
        for record in self:
            record.costo_total = record.cantidad * record.costo_unitario
    def get_compra(self):
        for record in self:
            #generar compra
            compra =  self.env['ventas.costos'].search([('purchase_id','!=',False),
                                                        ('sale_id','=',record.sale_id.id),
                                                         ('proveedor','=',record.proveedor.id)])
            #raise ValidationError(compra)

            if compra:
                compra= compra.purchase_id
                if compra:
                    compra.write({'partner_id': record.proveedor.id,'name':compra.name})
                    #orderline
                    line = self.env['purchase.order.line'].search([('product_id','=',record.producto.id),
                                                         ('order_id','=',compra.id)])
                    if line:
                        line.write({
                           'product_qty': record.cantidad,
                           'price_unit': record.costo_unitario,
                           'taxes_id'   : None,

                        })
                    else:
                        json_line = {
                           'product_id': record.producto.id,
                           'product_qty': record.cantidad,
                           'price_unit': record.costo_unitario,
                           'taxes_id'   : None,
                           'name': record.producto.name,
                           'order_id': compra.id,
                           'product_uom':record.producto.uom_id.id,
                           #'partner_id': record.proveedor.id,
                        }
                        date_p = fields.Datetime.now()
                        query_line = "INSERT INTO purchase_order_line (name,product_id,product_qty,price_unit," \
                             "product_uom,order_id,date_planned,price_subtotal,currency_id,company_id) " \
                             "VALUES('{}',{},{},{},{},{},'{}',{},{},{})".format(record.producto.name,record.producto.id,record.cantidad,
                                                               record.costo_unitario,record.producto.uom_id.id,compra.id,
                                                                     date_p,record.costo_total,record.moneda.id,
                                                                                compra.company_id.id)
                        #raise  ValidationError(query_line)
                        self.env.cr.execute(query_line)
                        record.purchase_id = compra.id
                        compra.amount_untaxed += record.costo_total
                        compra.amount_total += record.costo_total
                        #compra.order_line += self.env['purchase.order.line'].new(json_line)

            else:
                json_purchase = {
                   'partner_id': record.proveedor.id,
                   'currency_id': record.moneda.id
                }
                purchase = self.env['purchase.order'].create(json_purchase)
                json_line = {
                     'product_id': record.producto.id,
                     'product_qty': record.cantidad,
                     'price_unit': record.costo_unitario,
                     'taxes_id'   : None,
                     'name': record.producto.name,
                     'order_id': purchase.id,
                     'date_planned': fields.Datetime.now(),
                     'price_subtotal':record.costo_total
                     #'product_uom':record.producto.uom_id.id,
                     #'partner_id': record.proveedor.id,
                }

                date_p = fields.Datetime.now()

                query_line = "INSERT INTO purchase_order_line (name,product_id,product_qty,price_unit," \
                             "product_uom,order_id,date_planned,price_subtotal,currency_id,company_id) " \
                             "VALUES('{}',{},{},{},{},{},'{}',{},{},{})".format(record.producto.name,record.producto.id,record.cantidad,
                                                               record.costo_unitario,record.producto.uom_id.id,purchase.id,
                                                                     date_p,record.costo_total,record.moneda.id,
                                                                                purchase.company_id.id)
                self.env.cr.execute(query_line)
                import json
                #raise ValidationError(query_line)
                #raise ValidationError(json.dumps(json_line))
                #purchase.order_line += self.env['purchase.order.line'].new(json_line)
                line = self.env['purchase.order.line'].search([('product_id','=',record.producto.id),
                                                         ('order_id','=',purchase.id)])
                purchase.amount_untaxed += record.costo_total
                purchase.amount_total += record.costo_total
                #raise ValidationError(line)
                '''
                if line:
                    line.write({
                           'product_qty': record.cantidad,
                           'price_unit': record.costo_unitario,
                           'taxes_id'   : None,
                        })
                '''

                record.purchase_id = purchase.id




class SaleOrder(models.Model):
    _inherit = 'sale.order'
    costos   = fields.One2many('ventas.costos','sale_id')
    total_costos = fields.Float(compute="get_total",string='Costo Total')
    def get_total(self):
        for record in self:
            total = 0
            for cc  in record.costos:
                total += cc.costo_total
            record.total_costos = total



